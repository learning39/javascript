Documentation Officielle

# Installation
Il y'a 2 façons d'installer TypeScript :
- Via npm : npm install -g typescript
- Via Visual Studio plugin

# Type

# Fonction

function maFonction (nomParam : TypeParam) {
    //Code
    //return si necessaire
}

# Interface

interface Person {
    firstName: string;
    lastName: string;
}

function greeter(person: Person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}

let user = { firstName: "Jane", lastName: "User" }; //Déclaration d'un objet

On peut utiliser une interface en respectant juste sa forme sans passer par la clause 'implements'